﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextSearchBFS.Infrastructure.HtmlParser.Model;

namespace TextSearchBFS.HtmlParser.Model
{
    public class WebPage: IWebPage
    {
        public bool IsSuccess
        { get; set; }

        public string Content
        { get; set; }

        public string Url
        { get; set; }

        public string[] Urls
        { get; set; }
    }
}
