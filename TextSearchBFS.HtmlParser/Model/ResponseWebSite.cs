﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextSearchBFS.HtmlParser.Model
{
    internal struct ResponseWebSite
    {
        public readonly string Content;

        public readonly bool IsSuccess;

        public ResponseWebSite(string content, bool isSuccess)
        {
            this.Content = content;
            this.IsSuccess = isSuccess;
        }

    }
}
