﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using TextSearchBFS.HtmlParser.Model;
using TextSearchBFS.Infrastructure.HtmlParser.Model;
using TextSearchBFS.Infrastructure.HtmlParser.Service;

namespace TextSearchBFS.HtmlParser.Service
{
    public class PageParser : IPageParser
    {
        private string _baseUrl;
        private HtmlDocument _document;

        public PageParser()
        {
            _document = new HtmlDocument();
        }
        public async Task<IWebPage> GetPage(string url, string baseUrl)
        {
            this._baseUrl = baseUrl;
            var webPage = new WebPage();
            webPage.Url = url;
            var page = await GetContentWebPage(url);
           
            if (page.IsSuccess)
            {
                webPage.Content = ReplaceHtmlTags(page.Content);
                webPage.Urls = SearchReferences(page.Content);
                webPage.IsSuccess = true;
            }
            else
            {
                webPage.IsSuccess = false;
                webPage.Content = page.Content;
            }

            return webPage;
        }

        private async Task<ResponseWebSite> GetContentWebPage(string url)
        {
            return await Task<ResponseWebSite>.Factory.StartNew(() =>
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("user-agent",
                        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                    try
                    {
                        string html = client.DownloadString(url);

                        _document.LoadHtml(html);

                        var node = _document.DocumentNode.SelectSingleNode("//body");
                        string content = "";
                        if (node != null)
                        {
                            content = node.InnerHtml;
                        }
                      
                        ResponseWebSite response = new ResponseWebSite(content, true);
                        return response;
                    }
                    catch (Exception exception)
                    {
                        ResponseWebSite response = new ResponseWebSite(exception.Message, false);
                        return response;
                    }
                }
            });
        }

        private string[] SearchReferences(string htmlCode)
        {
            _document.LoadHtml(htmlCode);
            var nodes = _document.DocumentNode.SelectNodes("//a[@href]");
            if (nodes == null)
            {
                return new[] {""};
            }

            List<string> urls = new List<string>();

            for (int i = 0; i < nodes.Count; i++)
            {
                HtmlAttribute att = nodes[i].Attributes["href"];
                if (IsValidLink(att.Value))
                {
                    urls.Add(СorrectionUrl(att.Value));
                }
                
            }
 
            return urls.ToArray();
        }

        private bool IsValidLink(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return false;
            }
            if (url.Contains("javascript"))
            {
                return false;
            }

            return true;
        }

        private string СorrectionUrl(string url)
        {
            if (!url.Contains("http://"))
            {
                url = string.Format("http://{0}{1}", _baseUrl, url);
            }
            return url;
        }
        private string ReplaceHtmlTags(string text)
        {
            return Regex.Replace(text, "<[^>]+>", string.Empty).Replace(".", " ")
                .Replace("\n", " ")
                .Replace("\t", " ")
                .Replace(",", " ")
                .Replace("  ", " ")
                .Replace("‘", " ")
                .Replace("’", " ")
                .Replace("'", " ")
                .Replace("*", " ")
                .Replace(":", " ")
                .Replace("/", " ")
                .Replace("(", " ")
                .Replace(")", " ");
        }
    }
}
