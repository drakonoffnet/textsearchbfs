﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TextSearchBFS.Wpf.DTO
{
    public class TaskSchedulerDto
    {
      
        public string Url
        { get; set; }

 
        public int MaxCountThreads
        { get; set; }

 
        public string TextQuery
        { get; set; }

 
        public int MaxUrlScanned
        { get; set; }
    }
}