﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TextSearchBFS.Infrastructure.Application.Enum;

namespace TextSearchBFS.Wpf.DTO
{
    public class SiteTreeDto
    {
        public string UrlPage
        { get; set; }

        public PageStatus Status
        { get; set; }

        public string ParentUrl
        { get; set; }

        public int CountMatches
        { get; set; }

        public string ErorrMessage
        { get; set; }
    }
}