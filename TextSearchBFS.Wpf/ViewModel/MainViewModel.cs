﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TextSearchBFS.Application.Service;
using TextSearchBFS.Infrastructure.Application.Service;
using TextSearchBFS.Wpf.DTO;
using TaskScheduler = TextSearchBFS.Application.Model.TaskScheduler;

namespace TextSearchBFS.Wpf.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly ISchedulerService _schedulerService;
        private bool isBusy;

        private IEnumerable<SiteTreeDto> siteTreeDto;
        private Timer aTimer;

        private string url;
        private int maxCountThreads;
        private string textQuery;
        private int maxUrlScanned;
        private int pageCount;
        private string alertText;

        public ICommand StartCommand { get; private set; }
        public ICommand StopCommand { get; private set; }

        public MainViewModel()
        {
            _schedulerService = new SchedulerService();
            StartCommand = new RelayCommand(StartCrawler);
            StopCommand = new RelayCommand(StopCrawler);
            aTimer = new Timer(3000);
        }

        public string AlertText
        {
            get { return alertText; }
            set
            {
                alertText = value;
                RaisePropertyChanged("AlertText");
            }
        }

        public int PageCount
        {
            get { return pageCount;  }
            set
            {
                pageCount = value;
                RaisePropertyChanged("PageCount");
            }

        }
        public bool IsBusy
        {
            get { return !isBusy; }
            set
            {
                isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        public string Url
        {
            get { return url; }
            set
            {
                url = value;
                RaisePropertyChanged("Url");
            }
        }

        public int MaxCountThreads
        {
            get { return maxCountThreads; }
            set
            {
                maxCountThreads = value;
                RaisePropertyChanged("MaxCountThreads");
            }
        }

        public string TextQuery
        {
            get { return textQuery; }
            set
            {
                textQuery = value;
                RaisePropertyChanged("TextQuery");
            }
        }

        public int MaxUrlScanned
        {
            get { return maxUrlScanned; }
            set
            {
                maxUrlScanned = value;
                RaisePropertyChanged("MaxUrlScanned");
            }
        }

        public IEnumerable<SiteTreeDto> SiteTreeDto
        {
            get { return siteTreeDto; }
            set
            {
                siteTreeDto = value;
                RaisePropertyChanged("SiteTreeDto");
            }
        }



        private void StopCrawler()
        {
            if (!_schedulerService.IsDone)
            {
                _schedulerService.StopWorker();
            }
            _schedulerService.SiteTrees.Clear();
            aTimer.Elapsed -= UpdateResult;
            aTimer.Stop();
            IsBusy = false;
        }

        private async void StartCrawler()
        {
            var task = new TaskScheduler()
            {
                Url = Url,
                TextQuery = TextQuery,
                MaxUrlScanned = MaxUrlScanned,
                MaxCountThreads = MaxCountThreads
            };

            if (Validata(task))
            {
                aTimer.Elapsed += UpdateResult;
                aTimer.Start();
                IsBusy = true;

                try
                {
                    await _schedulerService.StartWorker(task);
                }
                catch (Exception exception)
                {
                    exception.Message.ToString();
                }
            }

        }

        private bool Validata(TaskScheduler task)
        {
            if (string.IsNullOrEmpty(task.Url))
            {
                AlertText = "needs to URL";
                return false;
            }

            if (string.IsNullOrEmpty(task.TextQuery))
            {
                AlertText = "needs to Text query";
                return false;
            }

            if (task.MaxCountThreads == 0)
            {
                AlertText = "Max count threads more than 0";
                return false;
            }
            if (task.MaxUrlScanned == 0)
            {
                AlertText = "Max Url scanned more than 0";
                return false;
            }
            AlertText = "";
            return true;
        }

        private void UpdateResult(object sender, ElapsedEventArgs e)
        {

            var result = _schedulerService.SiteTrees.Select(c => new SiteTreeDto()
            {
                ParentUrl = c.Value.ParentUrl,
                CountMatches = c.Value.CountMatches,
                ErorrMessage = c.Value.ErorrMessage,
                UrlPage = c.Value.UrlPage,
                Status = c.Value.Status
            });

            SiteTreeDto = result;
            PageCount = result.Count();
            IsBusy = true;

            if (_schedulerService.IsDone)
            {
                StopCrawler();
            }
        }
    }
}