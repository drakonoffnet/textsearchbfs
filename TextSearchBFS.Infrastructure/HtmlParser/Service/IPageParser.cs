﻿using System.Threading.Tasks;
using TextSearchBFS.Infrastructure.HtmlParser.Model;

namespace TextSearchBFS.Infrastructure.HtmlParser.Service
{
    public interface IPageParser
    {
        Task<IWebPage> GetPage(string url, string baseUr);
    }
}