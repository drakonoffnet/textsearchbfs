﻿namespace TextSearchBFS.Infrastructure.HtmlParser.Model
{
    public interface IWebPage
    {
        bool IsSuccess
        { get; set; }

        string Content
        { get; set; }

        string Url
        { get; set; }

        string[] Urls
        { get; set; }
    }
}