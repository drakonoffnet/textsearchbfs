﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextSearchBFS.Infrastructure.Application.Model
{
    public interface ITaskScheduler
    {
        string Url
        { get; set; }

        int MaxCountThreads
        { get; set; }

        string TextQuery
        { get; set; }

        int MaxUrlScanned
        { get; set; }
    }
}
