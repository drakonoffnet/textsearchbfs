﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextSearchBFS.Infrastructure.Application.Model
{
    public interface ISearchResults
    {
        int Count
        { get; set; }
    }
}
