﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TextSearchBFS.Infrastructure.Application.Model;

namespace TextSearchBFS.Infrastructure.Application.Service
{
    public interface ISchedulerService
    {
        void StopWorker();

        Dictionary<string, ISiteTree> SiteTrees
        { get; set; }

        bool IsDone
        { get; set; }

        Task StartWorker(ITaskScheduler taskScheduler);
    }
}
