﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextSearchBFS.Infrastructure.Application.Model;

namespace TextSearchBFS.Infrastructure.Application.Service
{
    public interface ISearchToolsService
    {
        Task<int> Matches(string text, string query);
    }
}
