﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextSearchBFS.Infrastructure.Application.Enum;

namespace TextSearchBFS.Infrastructure.Application.Service
{
    public interface ISiteTree
    {
        string UrlPage
        { get; set; }

        PageStatus Status
        { get; set; }

        string ParentUrl
        { get; set; }

        int CountMatches
        { get; set; }

        string ErorrMessage
        { get; set; }
    }
}
