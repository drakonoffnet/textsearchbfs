﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextSearchBFS.Infrastructure.Application.Enum
{
    public enum PageStatus : short
    {
        Start = 0,
        InProgress = 1,
        Success = 2,
        Erorr = 3,
    }
}
