﻿using System;
using TextSearchBFS.Application.Model;
using TextSearchBFS.HtmlParser.Model;
using TextSearchBFS.Infrastructure.Application.Enum;
using TextSearchBFS.Infrastructure.HtmlParser.Model;

namespace TextSearchBFS.Application.Service
{
    public partial class SchedulerService
    {
        private void SetStatusInProgress(string parentUrl, IWebPage rootPage)
        {
            var item = MappingData(parentUrl, rootPage, PageStatus.InProgress, 0);
            UpdateResult(item);
        }

        private void SetStatusStart(string url, string parentUrl)
        {
            var page = new WebPage()
            {
                Content = "",
                IsSuccess = true,
                Url = url
            };
            var item = MappingData(parentUrl, page, PageStatus.Start, 0);
            UpdateResult(item);
        }

        private void SetStatusErorr(string parentUrl, IWebPage rootPage)
        {
            var item = MappingData(parentUrl, rootPage, PageStatus.Erorr, 0);
            UpdateResult(item);
        }

        private void SetStatusSuccess(string parentUrl, IWebPage rootPage, int countMatches)
        {
            var item = MappingData(parentUrl, rootPage, PageStatus.Success, countMatches);
            UpdateResult(item);
        }

        private void UpdateResult(SiteTree item)
        {
            if (SiteTrees.Count == _maxUrlScanned)
            {
                StopWorker();
                return;
            }

            try
            {
                if (SiteTrees.ContainsKey(item.UrlPage))
                {
                    SiteTrees[item.UrlPage] = item;
                }
                else
                {
                    SiteTrees.Add(item.UrlPage, item);
                }
            }
            catch (Exception)
            {
            }

            if (_stopWork)
            {
                _queue.Clear();
            }
        }

        private SiteTree MappingData(string parentUrl, IWebPage rootPage, PageStatus status, int countMatches)
        {
            return new SiteTree()
            {
                Status = status,
                CountMatches = countMatches,
                UrlPage = rootPage.Url,
                ErorrMessage = !rootPage.IsSuccess ? rootPage.Content : "",
                ParentUrl = parentUrl
            };
        }
    }
}
