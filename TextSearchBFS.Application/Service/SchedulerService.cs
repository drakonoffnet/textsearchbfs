﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TextSearchBFS.Application.Model;
using TextSearchBFS.HtmlParser.Model;
using TextSearchBFS.HtmlParser.Service;
using TextSearchBFS.Infrastructure.Application.Enum;
using TextSearchBFS.Infrastructure.Application.Model;
using TextSearchBFS.Infrastructure.Application.Service;
using TextSearchBFS.Infrastructure.HtmlParser.Model;
using TextSearchBFS.Infrastructure.HtmlParser.Service;

namespace TextSearchBFS.Application.Service
{
    public partial class SchedulerService : ISchedulerService
    {
        public Dictionary<string, ISiteTree> SiteTrees { get; set; }
        public bool IsDone { get; set; }

        private readonly IPageParser _pageParser;
        private readonly ISearchToolsService _searchTools;
        private bool _stopWork { get; set; }
        private Uri _siteAddres;
        private int _maxUrlScanned;
        private int _maxCountThreads;
        private string _textQuery;
        private Task[] _tasks;
        private CancellationTokenSource _tokenSource;
        private Queue<QueueItem> _queue;

        public SchedulerService()
        {
            _tokenSource = new CancellationTokenSource();
            _pageParser = new PageParser();
            _searchTools = new SearchToolsService();
            _queue = new Queue<QueueItem>();
            _stopWork = false;
            IsDone = _stopWork;
            SiteTrees = new Dictionary<string, ISiteTree>();
        }

        public async Task StartWorker(ITaskScheduler taskScheduler)
        {
            _maxUrlScanned = taskScheduler.MaxUrlScanned;
            _maxCountThreads = taskScheduler.MaxCountThreads;
            _textQuery = taskScheduler.TextQuery;

            _siteAddres = new Uri(taskScheduler.Url);

            await Analysis(taskScheduler.Url);

            _tasks = new Task[_maxCountThreads];

            for (int i = 0; i < _maxCountThreads; i++)
            {
                AddTask(i);
            }

            while (!_stopWork)
            {
                if (!_queue.Any())
                {
                    break;
                }

                await Task.Delay(500);

                for (int i = 0; i < _tasks.Length; i++)
                {
                    var task = _tasks[i];
                    if (task.Status == TaskStatus.RanToCompletion)
                    {
                        AddTask(i);
                    }
                }

            }
        }

        public void StopWorker()
        {
            _stopWork = true;
            _queue.Clear();
            _tokenSource.Cancel();
            IsDone = _stopWork;
        }

        private void AddTask(int index)
        {
            var token = _tokenSource.Token;
            _tasks[index] = Task.Factory.StartNew(async () =>
            {
                if (_queue.Count > 0)
                {
                    var item = _queue.Dequeue();
                    await Analysis(item.PageUrl, item.ParentUrl);
                }
            }, token);
        }

        private async Task Analysis(string url, string parentUrl = "")
        {
            if (string.IsNullOrEmpty(url))
            {
                return;
            }
            SetStatusStart(url, parentUrl);

            var page = await _pageParser.GetPage(url, _siteAddres.Host);

            if (page.IsSuccess)
            {
                SetStatusInProgress(parentUrl, page);

                AddQueue(url, page.Urls);

                int countMatches = await _searchTools.Matches(page.Content, _textQuery);

                SetStatusSuccess(url, page, countMatches);
            }
            else
            {
                SetStatusErorr(url, page);
            }
        }

        private void AddQueue(string parentUrl, string[] urls)
        {
            foreach (var url in urls)
            {
                QueueItem item = new QueueItem(parentUrl, url);
                if (!_queue.Any(c => c.PageUrl.Contains(item.PageUrl)))
                {
                    _queue.Enqueue(item);
                }

            }
        }
    }
}
