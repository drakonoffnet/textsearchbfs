﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TextSearchBFS.Application.Model;
using TextSearchBFS.Infrastructure.Application.Model;
using TextSearchBFS.Infrastructure.Application.Service;

namespace TextSearchBFS.Application.Service
{
    public class SearchToolsService : ISearchToolsService
    {
        public async Task<int> Matches(string text, string query)
        {
            return await Task<int>.Factory.StartNew(() =>
            {
                string pattern = $@"(\W|^){query}(\W|$)";

                int count = new Regex(pattern).Matches(text).Count;

                return count;
            });
        }
    }
}
