﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextSearchBFS.Application.Model
{
    internal struct QueueItem
    {
        public string ParentUrl { get; set; }
        public string PageUrl { get; set; }

        public QueueItem(string parentUrl, string pageUrl)
        {
            this.ParentUrl = parentUrl;
            this.PageUrl = pageUrl;
        }
    }
}
