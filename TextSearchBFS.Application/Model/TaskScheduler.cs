﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextSearchBFS.Infrastructure.Application.Model;

namespace TextSearchBFS.Application.Model
{
    public class TaskScheduler : ITaskScheduler
    {
        public string Url
        { get; set; }

        public int MaxCountThreads
        { get; set; }

        public string TextQuery
        { get; set; }

        public int MaxUrlScanned
        { get; set; }
    }
}
