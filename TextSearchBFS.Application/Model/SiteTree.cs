﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextSearchBFS.Infrastructure.Application.Enum;
using TextSearchBFS.Infrastructure.Application.Service;

namespace TextSearchBFS.Application.Model
{
    public class SiteTree : ISiteTree
    {
        public string UrlPage
        { get; set; }

        public PageStatus Status
        { get; set; }

        public string ParentUrl
        { get; set; }

        public int CountMatches
        { get; set; }

        public string ErorrMessage
        { get; set; }
    }
}
