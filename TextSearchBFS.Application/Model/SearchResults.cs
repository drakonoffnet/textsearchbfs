﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextSearchBFS.Infrastructure.Application.Model;

namespace TextSearchBFS.Application.Model
{
    public class SearchResults  : ISearchResults
    {

        public int Count
        { get; set; }

    }
}
